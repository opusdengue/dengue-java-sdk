README

--------------------------------------  Objetivo  ---------------------------------------------
O objetivo do projeto dengue-java-sdk é propocionar uma API de requisições ao servidor do 
VazaDengue. A API permite recuperar os pontos de interesse (POI), para isso é feito uma requisição 
para o servidor, que por sua vez retorna um json como resposta. Esse json é deserializar como um 
objeto PointOfInterest e estará disponível para ser usado dentro da aplicação Android. O mesmo é 
executado com os demais endpoints (Tweets, Instagram e Picture). A API também permite o envio de 
pontos de interesses com e sem foto para o servidor do VazaDengue.



--------------------------------------  Execução  ---------------------------------------------
O projeto é compilado com o maven. Logo é só executar o maven install para limpar, compilar, testar
 (opcional) e deixar o .jar disponível para a aplicação Android.

Observação: Os casos de testes só devem ser executados se o projeto estiver configurado para 
acessar o servidor de desenvolvimento do VazaDengue. Para tal, o servidor precisa estar levantado e
a url de conexão precisa ser definida na classe SdkURIBuilder. Caso estiver usando a versão de 
produção (versão corrente) é necessário pular os testes. Para pular os testes é só usar a opção:
mvn install -DskipTests=true no terminal ou então configurar o Eclipse para pular os testes.


Leonardo