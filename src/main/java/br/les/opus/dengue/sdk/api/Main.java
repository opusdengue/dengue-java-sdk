package br.les.opus.dengue.sdk.api;

import br.les.opus.dengue.sdk.domain.InstagramEnvelope;
import br.les.opus.dengue.sdk.domain.PoiEnvelope;
import br.les.opus.dengue.sdk.domain.PointOfInterest;
import br.les.opus.dengue.sdk.domain.instagram.Instagram;
import br.les.opus.dengue.sdk.endpoints.PoiConsumer;


public class Main {

	public static void main(String[] args) {
		
		PoiConsumer consumer = new PoiConsumer();
		
		PoiEnvelope env = consumer.getAllPointsOfInterest();
		
		for(PointOfInterest p: env.getContent()){
			System.out.println(p);
		}
		
		InstagramEnvelope instagramEnvelope = consumer.getAllInstagramPosts();
		
		for(Instagram instagram: instagramEnvelope.getContent()){
			System.out.println(instagram);
		}
	}

}
