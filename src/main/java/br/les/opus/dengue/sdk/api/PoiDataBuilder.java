package br.les.opus.dengue.sdk.api;

import br.les.opus.dengue.sdk.domain.PoiEnvelope;
import br.les.opus.dengue.sdk.domain.PointOfInterest;
import br.les.opus.dengue.sdk.endpoints.PoiConsumer;

public class PoiDataBuilder {
	private PoiConsumer consumer;
	
	public PoiDataBuilder() {
		consumer = new PoiConsumer();
	}
	
	
	public void printPois(){
		PoiEnvelope pois = consumer.getAllPointsOfInterest();
		
		for(PointOfInterest poi: pois.getContent()){
			System.out.println(poi.getId());
		}

	}
	

}
