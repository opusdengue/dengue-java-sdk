package br.les.opus.dengue.sdk.api;

public class SdkDengueException extends RuntimeException {

	private static final long serialVersionUID = -6958064024355100975L;
	
	public SdkDengueException(String message) {
		super(message);
	}
	
	public SdkDengueException(Throwable cause) {
		super(cause);
	}
	
	public SdkDengueException(String message, Throwable cause) {
		super(message, cause);
	}


}
