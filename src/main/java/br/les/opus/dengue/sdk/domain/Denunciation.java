package br.les.opus.dengue.sdk.domain;

import br.les.opus.dengue.sdk.domain.user.User;


public class Denunciation{
	private Long id;
	
	private User user;
	
	private String description;
	
	private PointOfInterest poi;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public PointOfInterest getPoi() {
		return poi;
	}

	public void setPoi(PointOfInterest poi) {
		this.poi = poi;
	}
	
}
