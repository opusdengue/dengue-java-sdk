package br.les.opus.dengue.sdk.domain;

import java.util.List;

public class Envelope<T> {
	private Link link;
	
	private Page page;
	
	private List<T> content;

	public Link getLink() {
		return link;
	}

	public void setLink(Link link) {
		this.link = link;
	}
 
	public Page getPage() {
		return page;
	}

	public void setPage(Page page) {
		this.page = page;
	}

	public List<T> getContent() {
		return content;
	}

	public void setContent(List<T> content) {
		this.content = content;
	}

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer("Envelope [" + link + "\n" + ", " + page + "\n" + ", ");
		for (T t: content){
			buffer.append("\n" + t);
		}
		buffer.append("]");
		
		return buffer.toString();
	}
	
	 

}

