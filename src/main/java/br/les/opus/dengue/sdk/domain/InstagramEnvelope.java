package br.les.opus.dengue.sdk.domain;

import br.les.opus.dengue.sdk.domain.instagram.Instagram;

public class InstagramEnvelope extends Envelope<Instagram>{
	@Override
	public String toString() {
		return "InstagramEnvelope ["
				+ super.toString() + "]";
	}
}
