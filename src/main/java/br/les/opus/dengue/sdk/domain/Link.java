package br.les.opus.dengue.sdk.domain;

public class Link {
	private String rel;
	
	private String link;
	
	public String getRel() {
		return rel;
	}
	
	public void setRel(String rel) {
		this.rel = rel;
	}
	
	public String getLink() {
		return link;
	}
	
	public void setLink(String link) {
		this.link = link;
	}

	@Override
	public String toString() {
		return "Link [rel=" + rel + ", link=" + link + "]";
	}
	

}
