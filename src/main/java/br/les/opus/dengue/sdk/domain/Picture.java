package br.les.opus.dengue.sdk.domain;

import java.util.Date;

import br.les.opus.dengue.sdk.domain.user.User;


public class Picture {
	
	private Long id;
	
	private String fileName;
	
	/**
	 * Document creation date
	 */
	private Date date;
	
	private User user;
	
	private PointOfInterest poi;
	
	private String mimeType;
	
	private Integer width;
	
	private Integer height;
	
	
	public Picture() {
		this.date = new Date();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User author) {
		this.user = author;
	}

	public PointOfInterest getPoi() {
		return poi;
	}

	public void setPoi(PointOfInterest poi) {
		this.poi = poi;
	}

	public String getMimeType() {
		return mimeType;
	}

	public void setContentType(String mimeType) {
		this.mimeType = mimeType;
	}

	public Integer getWidth() {
		return width;
	}

	public void setWidth(Integer width) {
		this.width = width;
	}

	public Integer getHeight() {
		return height;
	}

	public void setHeight(Integer height) {
		this.height = height;
	}

	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}
	
	@Override
	public String toString() {
		return ""
				+ "id: " + getId()
				+ "fileName: " + getFileName()
				+ "date: " + getDate();
	}
	
}
