package br.les.opus.dengue.sdk.domain;


public class PoiEnvelope extends Envelope<PointOfInterest>{

	@Override
	public String toString() {
		return "PoiEnvelope ["
				+ super.toString() + "]";
	}
}
