package br.les.opus.dengue.sdk.domain;

import java.util.List;

import br.les.opus.dengue.sdk.domain.fields.Field;


public class PoiType{
	private Long id;
	
	private String name;
	
	private List<Field> fields;
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public List<Field> getFields() {
		return fields;
	}

	public void setFields(List<Field> fields) {
		this.fields = fields;
	}
	
	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		for (Field f: fields){
			buffer.append("\n" + f);
		}
		return "id: " + id + " name: " + name + " fields: " + buffer;
	}
	
}
