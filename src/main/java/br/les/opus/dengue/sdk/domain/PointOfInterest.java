package br.les.opus.dengue.sdk.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.les.opus.dengue.sdk.domain.fields.FieldValue;
import br.les.opus.dengue.sdk.domain.user.User;


public class PointOfInterest {
	private String address;
	
	private Long id;
	
    private LatLng location;
	
	private String title;
	
	private String description;
	
	private Date date;
	
	private PoiType type;
	
	private User user;
	
	private List<Picture> pictures;
	
	private List<FieldValue> fieldValues;
	
	private List<Denunciation> denunciations;

	/**
	 * Whether the POI is visible to public or not
	 */
	private Boolean published;
	
	public PointOfInterest() {
		this.date = new Date();
		this.published = true;
		this.pictures = new ArrayList<>();
	}
	
	

	public String getAddress() {
		return address;
	}



	public void setAddress(String address) {
		this.address = address;
	}



	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LatLng getLocation() {
		return location;
	}

	public void setLocation(LatLng location) {
		this.location = location;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public PoiType getType() {
		return type;
	}

	public void setType(PoiType type) {
		this.type = type;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Boolean getPublished() {
		return published;
	}

	public void setPublished(Boolean published) {
		this.published = published;
	}

	public List<Picture> getPictures() {
		return pictures;
	}

	public void setPictures(List<Picture> documents) {
		this.pictures = documents;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<Denunciation> getDenunciations() {
		return denunciations;
	}

	public void setDenunciations(List<Denunciation> denunciations) {
		this.denunciations = denunciations;
	}
	
	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer(); 
		buffer.append(
				"\ntitle: " + title
				+ "description: " + description
				+ "\ndate: " + date
				+ "\n\n");
		
		buffer.append("Type:\n" + type.getName() + "\n");
		
		List<FieldValue> fieldValues = getFieldValues();
		if(fieldValues != null){
			for(FieldValue fv: getFieldValues()){
				buffer.append(fv);
			}
		}
		
		return buffer.toString();
		
	}

	public List<FieldValue> getFieldValues() {
		return fieldValues;
	}

	public void setFieldValues(List<FieldValue> fieldValues) {
		this.fieldValues = fieldValues;
	}
	
}
