package br.les.opus.dengue.sdk.domain;

import java.util.Date;


public class Tweet{
	private Long id;
	
	private Date createdAt;
	
	private int favoriteCount;
	
	private LatLng geolocation;
	
	private String inReplyToScreenName;
	
	private Long inReplyToStatusId;
	
	private Long inReplyToUserId;
	
	private String lang;
	
	private Integer retweetCount;
	
	private String text;
	
	private String source;
	
	private Boolean favorited;
	
	private Boolean possiblySensitive;
	
	private Boolean retweet;
	
	private Boolean retweeted;
	
	private Boolean isTruncated;
	
	private TwitterUser user;
	
	private TweetClassification classification;
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public int getFavoriteCount() {
		return favoriteCount;
	}

	public void setFavoriteCount(int favoriteCount) {
		this.favoriteCount = favoriteCount;
	}

	public String getInReplyToScreenName() {
		return inReplyToScreenName;
	}

	public void setInReplyToScreenName(String inReplyToScreenName) {
		this.inReplyToScreenName = inReplyToScreenName;
	}

	public Long getInReplyToStatusId() {
		return inReplyToStatusId;
	}

	public void setInReplyToStatusId(Long inReplyToStatusId) {
		this.inReplyToStatusId = inReplyToStatusId;
	}

	public Long getInReplyToUserId() {
		return inReplyToUserId;
	}

	public void setInReplyToUserId(Long inReplyToUserId) {
		this.inReplyToUserId = inReplyToUserId;
	}

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public Integer getRetweetCount() {
		return retweetCount;
	}

	public void setRetweetCount(Integer retweetCount) {
		this.retweetCount = retweetCount;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public Boolean getFavorited() {
		return favorited;
	}

	public void setFavorited(Boolean favorited) {
		this.favorited = favorited;
	}

	public Boolean getPossiblySensitive() {
		return possiblySensitive;
	}

	public void setPossiblySensitive(Boolean possiblySensitive) {
		this.possiblySensitive = possiblySensitive;
	}

	public Boolean getRetweet() {
		return retweet;
	}

	public void setRetweet(Boolean retweet) {
		this.retweet = retweet;
	}

	public Boolean getRetweeted() {
		return retweeted;
	}

	public void setRetweeted(Boolean retweeted) {
		this.retweeted = retweeted;
	}

	public Boolean getIsTruncated() {
		return isTruncated;
	}

	public void setIsTruncated(Boolean isTruncated) {
		this.isTruncated = isTruncated;
	}

	

	public LatLng getGeolocation() {
		return geolocation;
	}

	public void setGeolocation(LatLng geolocation) {
		this.geolocation = geolocation;
	}

	public TwitterUser getUser() {
		return user;
	}

	public void setUser(TwitterUser user) {
		this.user = user;
	}
	
	public TweetClassification getClassification() {
		return classification;
	}

	public void setClassification(TweetClassification classification) {
		this.classification = classification;
	}

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("Tweet:\n"
				+ text + ",\n"
				+ "created at =" + createdAt);
		
		if(classification != null){
			buffer.append(classification);
		}
		
		if(user != null){
			buffer.append(user);
		}
		
		return buffer.toString();
	}
	

}
