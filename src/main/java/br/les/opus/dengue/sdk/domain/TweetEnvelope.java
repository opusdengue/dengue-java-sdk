package br.les.opus.dengue.sdk.domain;

public class TweetEnvelope extends Envelope<Tweet>{
	@Override
	public String toString() {
		return "TweetEnvelope ["
				+ super.toString() + "]";
	}
}
