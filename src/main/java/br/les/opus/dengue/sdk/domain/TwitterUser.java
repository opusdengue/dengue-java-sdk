package br.les.opus.dengue.sdk.domain;

import java.util.Date;
public class TwitterUser {
	private Long id;

	private String name;
	
	private String screenName;
	
	private String location;
	
	private String description;
	
	private Boolean isContributorsEnabled;

	private String profileImageUrl;
	
	private String profileImageUrlHttps;
	
	private String url;
	
	private Boolean isProtected;
	
	private Integer followersCount;

	private String profileBackgroundColor;

	private String profileTextColor;
	
	private String profileLinkColor;
	
	private String profileSidebarFillColor;
	
	private String profileSidebarBorderColor;
	
	private Boolean profileUseBackgroundImage;
	
	private Boolean showAllInlineMedia;
	
	private Integer friendsCount;
	
	private Date createdAt;
	
	private Integer favouritesCount;
	
	private Integer utcOffset;
	
	private String timeZone;
	
	private String profileBackgroundImageUrl;
	
	private String profileBackgroundImageUrlHttps;
	
	private String profileBannerImageUrl;
	
	private Boolean profileBackgroundTiled;
	
	private String lang;
	
	private Integer statusesCount;
	
	private Boolean isGeoEnabled;
	
	private Boolean isVerified;
	
	private Boolean translator;
	
	private Integer listedCount;
	
	private Boolean isFollowRequestSent;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getScreenName() {
		return screenName;
	}

	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getIsContributorsEnabled() {
		return isContributorsEnabled;
	}

	public void setIsContributorsEnabled(Boolean isContributorsEnabled) {
		this.isContributorsEnabled = isContributorsEnabled;
	}

	public String getProfileImageUrl() {
		return profileImageUrl;
	}

	public void setProfileImageUrl(String profileImageUrl) {
		this.profileImageUrl = profileImageUrl;
	}

	public String getProfileImageUrlHttps() {
		return profileImageUrlHttps;
	}

	public void setProfileImageUrlHttps(String profileImageUrlHttps) {
		this.profileImageUrlHttps = profileImageUrlHttps;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Boolean getIsProtected() {
		return isProtected;
	}

	public void setIsProtected(Boolean isProtected) {
		this.isProtected = isProtected;
	}

	public Integer getFollowersCount() {
		return followersCount;
	}

	public void setFollowersCount(Integer followersCount) {
		this.followersCount = followersCount;
	}

	public String getProfileBackgroundColor() {
		return profileBackgroundColor;
	}

	public void setProfileBackgroundColor(String profileBackgroundColor) {
		this.profileBackgroundColor = profileBackgroundColor;
	}

	public String getProfileTextColor() {
		return profileTextColor;
	}

	public void setProfileTextColor(String profileTextColor) {
		this.profileTextColor = profileTextColor;
	}

	public String getProfileLinkColor() {
		return profileLinkColor;
	}

	public void setProfileLinkColor(String profileLinkColor) {
		this.profileLinkColor = profileLinkColor;
	}

	public String getProfileSidebarFillColor() {
		return profileSidebarFillColor;
	}

	public void setProfileSidebarFillColor(String profileSidebarFillColor) {
		this.profileSidebarFillColor = profileSidebarFillColor;
	}

	public String getProfileSidebarBorderColor() {
		return profileSidebarBorderColor;
	}

	public void setProfileSidebarBorderColor(String profileSidebarBorderColor) {
		this.profileSidebarBorderColor = profileSidebarBorderColor;
	}

	public Boolean getProfileUseBackgroundImage() {
		return profileUseBackgroundImage;
	}

	public void setProfileUseBackgroundImage(Boolean profileUseBackgroundImage) {
		this.profileUseBackgroundImage = profileUseBackgroundImage;
	}

	public Boolean getShowAllInlineMedia() {
		return showAllInlineMedia;
	}

	public void setShowAllInlineMedia(Boolean showAllInlineMedia) {
		this.showAllInlineMedia = showAllInlineMedia;
	}

	public Integer getFriendsCount() {
		return friendsCount;
	}

	public void setFriendsCount(Integer friendsCount) {
		this.friendsCount = friendsCount;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Integer getFavouritesCount() {
		return favouritesCount;
	}

	public void setFavouritesCount(Integer favouritesCount) {
		this.favouritesCount = favouritesCount;
	}

	public Integer getUtcOffset() {
		return utcOffset;
	}

	public void setUtcOffset(Integer utcOffset) {
		this.utcOffset = utcOffset;
	}

	public String getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

	public String getProfileBackgroundImageUrl() {
		return profileBackgroundImageUrl;
	}

	public void setProfileBackgroundImageUrl(String profileBackgroundImageUrl) {
		this.profileBackgroundImageUrl = profileBackgroundImageUrl;
	}

	public String getProfileBackgroundImageUrlHttps() {
		return profileBackgroundImageUrlHttps;
	}

	public void setProfileBackgroundImageUrlHttps(
			String profileBackgroundImageUrlHttps) {
		this.profileBackgroundImageUrlHttps = profileBackgroundImageUrlHttps;
	}

	public String getProfileBannerImageUrl() {
		return profileBannerImageUrl;
	}

	public void setProfileBannerImageUrl(String profileBannerImageUrl) {
		this.profileBannerImageUrl = profileBannerImageUrl;
	}

	public Boolean getProfileBackgroundTiled() {
		return profileBackgroundTiled;
	}

	public void setProfileBackgroundTiled(Boolean profileBackgroundTiled) {
		this.profileBackgroundTiled = profileBackgroundTiled;
	}

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public Integer getStatusesCount() {
		return statusesCount;
	}

	public void setStatusesCount(Integer statusesCount) {
		this.statusesCount = statusesCount;
	}

	public Boolean getIsGeoEnabled() {
		return isGeoEnabled;
	}

	public void setIsGeoEnabled(Boolean isGeoEnabled) {
		this.isGeoEnabled = isGeoEnabled;
	}

	public Boolean getIsVerified() {
		return isVerified;
	}

	public void setIsVerified(Boolean isVerified) {
		this.isVerified = isVerified;
	}

	public Boolean getTranslator() {
		return translator;
	}

	public void setTranslator(Boolean translator) {
		this.translator = translator;
	}

	public Integer getListedCount() {
		return listedCount;
	}

	public void setListedCount(Integer listedCount) {
		this.listedCount = listedCount;
	}

	public Boolean getIsFollowRequestSent() {
		return isFollowRequestSent;
	}

	public void setIsFollowRequestSent(Boolean isFollowRequestSent) {
		this.isFollowRequestSent = isFollowRequestSent;
	}

	@Override
	public String toString() {
		return "\n\nTwitter User:" + screenName +
				"\nName: " + name + "\n"
				+ "URL: " + profileImageUrl + "\n";
	}
}
