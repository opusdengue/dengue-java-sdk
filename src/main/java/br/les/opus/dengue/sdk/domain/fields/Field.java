package br.les.opus.dengue.sdk.domain.fields;

import java.util.List;

public class Field {
	public Field(){}
	
	public Field(Long id, String name){
		this.id = id;
		this.name = name;
	}
	
	private Long id;
	
	private String name;
	
	private String helpText;
	
	private Boolean required;
	
	private FieldType type;
	
	private List<FieldOption> options;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getRequired() {
		return required;
	}

	public void setRequired(Boolean required) {
		this.required = required;
	}

	public FieldType getType() {
		return type;
	}

	public void setType(FieldType type) {
		this.type = type;
	}

	public String getHelpText() {
		return helpText;
	}

	public void setHelpText(String helpText) {
		this.helpText = helpText;
	}

	public List<FieldOption> getOptions() {
		return options;
	}

	public void setOptions(List<FieldOption> options) {
		this.options = options;
	}

	@Override
	public String toString() {
		return "Field [id=" + id + ", name=" + name + ", helpText=" + helpText
				+ ", required=" + required + ", type=" + type + ", options="
				+ options + "]";
	}
	
	

}
