package br.les.opus.dengue.sdk.domain.fields;

import java.io.Serializable;

public class FieldType implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2591627628333257532L;
	private Long id;
	private String name;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
