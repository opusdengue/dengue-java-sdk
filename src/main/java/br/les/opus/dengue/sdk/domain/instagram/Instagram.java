package br.les.opus.dengue.sdk.domain.instagram;

import java.util.Date;

public class Instagram {
	private String id;

	private String type; //image or video

	private String filter;	

	private Comment caption;
	
	private String link;
	private InstagramUser user;
	
	private Date createdTime;
	
	private PicturePool images;
	
	private VideoPool videos;
	private Location location;
	
	
	public boolean isValid() {
		if (images == null && videos == null) {
			return false;
		}
		if (images != null && !images.isValid()) {
			return false;
		}
		if (videos != null && !videos.isValid()) {
			return false;
		}
		return true; 
	}
	
	public void replaceImages(PicturePool oldPool) {
		if (this.images == null) {
			return;
		}
		images.setId(oldPool.getId());
		images.getLowResolution().setId(oldPool.getLowResolution().getId());
		images.getStandardResolution().setId(oldPool.getStandardResolution().getId());
		images.getThumbnail().setId(oldPool.getThumbnail().getId());
	}
	
	public void replaceVideos(VideoPool oldVideoPool) {
		if (this.videos == null) {
			return;
		}
		videos.setId(oldVideoPool.getId());
		videos.getLowResolution().setId(oldVideoPool.getLowResolution().getId());
		videos.getStandardResolution().setId(oldVideoPool.getStandardResolution().getId());
	}
	

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getFilter() {
		return filter;
	}

	public void setFilter(String filter) {
		this.filter = filter;
	}

	public Comment getCaption() {
		return caption;
	}

	public void setCaption(Comment caption) {
		this.caption = caption;
	}


	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public InstagramUser getUser() {
		return user;
	}

	public void setUser(InstagramUser user) {
		this.user = user;
	}

	public Date getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}

	public PicturePool getImages() {
		return images;
	}

	public void setImages(PicturePool images) {
		this.images = images;
	}

	public VideoPool getVideos() {
		return videos;
	}

	public void setVideos(VideoPool videos) {
		this.videos = videos;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	@Override
	public String toString() {
		return "Instagram [id=" + id + ", type=" + type + ", caption="
				+ caption + ", image= " + images + "]";
	}
	
	

}
