package br.les.opus.dengue.sdk.domain.instagram;

public class InstagramPicture  {

	private Long id;

	private String url;

	private Integer width;

	private Integer height;
	
	public boolean isValid() {
		return !(url == null || url.equals(""));
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Integer getWidth() {
		return width;
	}

	public void setWidth(Integer width) {
		this.width = width;
	}

	public Integer getHeight() {
		return height;
	}

	public void setHeight(Integer height) {
		this.height = height;
	}

}
