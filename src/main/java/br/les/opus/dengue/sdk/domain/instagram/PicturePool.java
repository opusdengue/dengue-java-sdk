package br.les.opus.dengue.sdk.domain.instagram;


public class PicturePool {
	private Long id;

	private InstagramPicture lowResolution;
	
	private InstagramPicture thumbnail;
	
	private InstagramPicture standardResolution;
	
	public boolean isValid() {
		return  lowResolution != null && lowResolution.isValid()
				&& standardResolution != null && standardResolution.isValid()
				&& thumbnail != null && thumbnail.isValid();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public InstagramPicture getLowResolution() {
		return lowResolution;
	}

	public void setLowResolution(InstagramPicture lowResolution) {
		this.lowResolution = lowResolution;
	}

	public InstagramPicture getThumbnail() {
		return thumbnail;
	}

	public void setThumbnail(InstagramPicture thumbnail) {
		this.thumbnail = thumbnail;
	}

	public InstagramPicture getStandardResolution() {
		return standardResolution;
	}

	public void setStandardResolution(InstagramPicture standardResolution) {
		this.standardResolution = standardResolution;
	}

	@Override
	public String toString() {
		return "[" + standardResolution.getUrl() + "]";
	}
	
	
	
}
