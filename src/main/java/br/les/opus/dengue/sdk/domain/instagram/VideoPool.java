package br.les.opus.dengue.sdk.domain.instagram;

public class VideoPool {
	private Long id;
	private Video lowResolution;
	private Video standardResolution;
	public boolean isValid() {
		return  lowResolution != null && lowResolution.isValid()
				&& standardResolution != null && standardResolution.isValid();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Video getLowResolution() {
		return lowResolution;
	}

	public void setLowResolution(Video lowResolution) {
		this.lowResolution = lowResolution;
	}

	public Video getStandardResolution() {
		return standardResolution;
	}

	public void setStandardResolution(Video standardResolution) {
		this.standardResolution = standardResolution;
	}
}
