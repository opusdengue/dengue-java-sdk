package br.les.opus.dengue.sdk.domain.user;

import java.util.ArrayList;
import java.util.List;


public class Role {
	
	public static final String ROOT = "ROOT";

	private Long id;
	
	private String authority;
	
	private List<RoleResource> roleResources;
	
	private Role parent;

	/**
	 * Retorna a lista de todos os recursos associados ao papel
	 * e aos pais dele
	 * @return
	 */
	public List<Resource> getAllResources() {
		List<Resource> resources = new ArrayList<Resource>();
		Role role = this;
		while (role != null) {
			List<RoleResource> roleResources = role.getRoleResources();
			for (RoleResource roleResource : roleResources) {
				resources.add(roleResource.getResource());
			}
			role = role.getParent();
		}
		return resources;
	}
	
	public String getAuthority() {
		return authority;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setAuthority(String authority) {
		this.authority = authority;
	}

	public Role getParent() {
		return parent;
	}

	public void setParent(Role parent) {
		this.parent = parent;
	}

	public List<RoleResource> getRoleResources() {
		return roleResources;
	}

	public void setRoleResources(List<RoleResource> roleResources) {
		this.roleResources = roleResources;
	}

	@Override
	public String toString() {
		return "Role [id=" + id + ", authority=" + authority + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Role other = (Role) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}
