package br.les.opus.dengue.sdk.endpoints;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URI;

import br.les.opus.dengue.sdk.api.SdkDengueException;
import br.les.opus.dengue.sdk.util.SdkURIBuilder;

public abstract class EndpointConsumer {
	public static String PORTUGUESE = "pt";
	public static String ENGLISH = "en";

	public EndpointConsumer() {
	}

	
	protected SdkURIBuilder getURIBuilder() {
		SdkURIBuilder builder = new SdkURIBuilder();
		
		return builder;
	}
	
	protected String executeGetRequest(URI uri, String language) throws IOException{
		HttpURLConnection urlConnection = (HttpURLConnection) uri.toURL().openConnection();
	    urlConnection.setRequestProperty("Accept-Language", language);
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		
		InputStream input;
		try{
			input = urlConnection.getInputStream();
			readStream(input, output);
		    output.close();
		    input.close();
		    
			
		}finally{
			urlConnection.disconnect();
		}
		
		return new String(output.toByteArray(), "utf-8");
	}
	
	
	protected String executePostRequest(URI uri, String json) throws IOException{
		HttpURLConnection urlConnection = (HttpURLConnection) uri.toURL().openConnection();
		String response;
		
		try {
			urlConnection.setDoOutput(true);
			urlConnection.setChunkedStreamingMode(0);
			urlConnection.setRequestMethod("POST");
			urlConnection.setRequestProperty("Content-Type", "application/json;charset=utf-8"); 
		    urlConnection.setRequestProperty("charset", "UTF-8");
		    
		    DataOutputStream wr = new DataOutputStream(urlConnection.getOutputStream ());
		    wr.write(json.getBytes());
		    
		    wr.flush();
		    wr.close();
		    
		    ByteArrayOutputStream output = new ByteArrayOutputStream();
			InputStream input = urlConnection.getInputStream();
			readStream(input, output);
			
			int status = urlConnection.getResponseCode();
			
			if(status != HttpURLConnection.HTTP_CREATED && status != HttpURLConnection.HTTP_OK){
				throw new SdkDengueException("Unexpected response: " + status);
			}
		    
			response = new String(output.toByteArray(), "utf-8");
			
			output.close();
		    input.close();
			
		}finally {
			urlConnection.disconnect();
		}
		
		return response;

	}
	
	protected String executePostPictureRequest(URI uri, String input) throws IOException{
		HttpURLConnection urlConnection = (HttpURLConnection) uri.toURL().openConnection();
		//ByteArrayOutputStream output = new ByteArrayOutputStream();
		//readStream(input, output);
		
		String response;
		
		try{
			urlConnection.setUseCaches(false);
			urlConnection.setDoOutput(true);

			urlConnection.setRequestMethod("POST");
			urlConnection.setRequestProperty("Connection", "Keep-Alive");
			urlConnection.setRequestProperty("Cache-Control", "no-cache");
			urlConnection.setRequestProperty("Content-Type", "multipart/form-data");
			
						
			DataOutputStream data = new DataOutputStream(urlConnection.getOutputStream ());
			data.writeBytes(input);
			
			data.flush();
		    data.close();
		    
		    ByteArrayOutputStream outputResponse = new ByteArrayOutputStream();
			InputStream inputResponse = urlConnection.getInputStream();
			readStream(inputResponse, outputResponse);
			
			response = new String(outputResponse.toByteArray(), "utf-8");
		    
			outputResponse.close();
		    inputResponse.close();
						
		}finally{
			urlConnection.disconnect();
		}
		
		return response;
	}
	
	/*@SuppressWarnings("deprecation")
	protected HttpResponse executeRequest(HttpUriRequest request) throws IOException {
		
		HttpClient client = new DefaultHttpClient();
		HttpResponse response = client.execute(request);
		return response;
	}*/

	protected void readStream(InputStream input, ByteArrayOutputStream output) throws IOException{
		int bytesRead;
		
	    while ((bytesRead = input.read()) != -1){
	        output.write(bytesRead);
	    }
	}
		
		
		
}

