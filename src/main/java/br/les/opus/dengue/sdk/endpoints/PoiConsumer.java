package br.les.opus.dengue.sdk.endpoints;

import java.io.IOException;
import java.net.URISyntaxException;

import br.les.opus.dengue.sdk.api.SdkDengueException;
import br.les.opus.dengue.sdk.domain.InstagramEnvelope;
import br.les.opus.dengue.sdk.domain.PoiEnvelope;
import br.les.opus.dengue.sdk.domain.PointOfInterest;
import br.les.opus.dengue.sdk.domain.TweetEnvelope;
import br.les.opus.dengue.sdk.util.GsonDengueBuilder;
import br.les.opus.dengue.sdk.util.SdkURIBuilder;

public class PoiConsumer extends EndpointConsumer{
	private String language;
	
	public PoiConsumer(){
		super();
		language = "pt";
	}
	
	public PoiConsumer(String language) {
		super();
		this.language = language;
	}
	
	
	/*
	 * Get all point of interest
	 */
	public PoiEnvelope getAllPointsOfInterest(){
		String json ="";
		
		SdkURIBuilder uriBuilder = getURIBuilder();
		uriBuilder.setPathPoi(SdkURIBuilder.POI);
		uriBuilder.setPathPoi(SdkURIBuilder.ORDERBYDATA);
				
		try {
			json = executeGetRequest(uriBuilder.build(), language);
		} catch (IOException e) {
			throw new SdkDengueException("An exception was thrown during"
					+ " request", e);
		} catch (URISyntaxException e) {
			throw new SdkDengueException("An exception was thrown during"
					+ " uri creation", e);
		}
		
		return new GsonDengueBuilder().build().fromJson(json,PoiEnvelope.class);		
	}
	
	/*
	 * Get an specific poi
	 */
	public PointOfInterest getPointofInterest(long id){
		String json = "";
		
		SdkURIBuilder uriBuilder = getURIBuilder();
		uriBuilder.setPathPoi(SdkURIBuilder.POI);
		
		uriBuilder.setPathPoi(Long.toString(id));
		
		try {
			json = executeGetRequest(uriBuilder.build(), language);
		} catch (IOException e) {
			throw new SdkDengueException("An exception was thrown during"
					+ " request", e);
		} catch (URISyntaxException e) {
			throw new SdkDengueException("An exception was thrown during"
					+ " uri creation", e);
		}
		
		return new GsonDengueBuilder().build().fromJson(json, PointOfInterest.class);
	}
	
	
	/*
	 * Get all the tweets as pois
	 */
	public TweetEnvelope getAllTweets(){
		String json ="";
		
		SdkURIBuilder uriBuilder = getURIBuilder();
		uriBuilder.setPathPoi(SdkURIBuilder.TWEETS);
				
		try {
			json = executeGetRequest(uriBuilder.build(), language);
		} catch (IOException e) {
			throw new SdkDengueException("An exception was thrown during"
					+ " tweets request", e);
		} catch (URISyntaxException e) {
			throw new SdkDengueException("An exception was thrown during"
					+ " uri creation", e);
		}
		//Type type = new TypeToken<List<Tweet>>(){}.getType();
		
		return new GsonDengueBuilder().build().fromJson(json,TweetEnvelope.class);	
	}
	
	/*
	 * Get all the instagram posts as pois
	 */
	public InstagramEnvelope getAllInstagramPosts(){
		String json ="";
		
		SdkURIBuilder uriBuilder = getURIBuilder();
		uriBuilder.setPathPoi(SdkURIBuilder.INSTAGRAM);
				
		try {
			json = executeGetRequest(uriBuilder.build(), language);
		} catch (IOException e) {
			throw new SdkDengueException("An exception was thrown during"
					+ " Instagram request", e);
		} catch (URISyntaxException e) {
			throw new SdkDengueException("An exception was thrown during"
					+ " uri creation", e);
		}
		//Type type = new TypeToken<List<Tweet>>(){}.getType();
		
		return new GsonDengueBuilder().build().fromJson(json,InstagramEnvelope.class);	
		
	}
}
