package br.les.opus.dengue.sdk.endpoints;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Date;

import br.les.opus.dengue.sdk.api.SdkDengueException;
import br.les.opus.dengue.sdk.domain.PointOfInterest;
import br.les.opus.dengue.sdk.util.DateJsonSerializer;
import br.les.opus.dengue.sdk.util.GsonDengueBuilder;
import br.les.opus.dengue.sdk.util.SdkURIBuilder;

import com.google.gson.GsonBuilder;

public class PoiProducer extends EndpointConsumer{
	
	public PoiProducer() {
		super();
	}
	
	
	public PointOfInterest sendPoiAnonymous(PointOfInterest poi){
		PointOfInterest retPoi = null;
		URI uri;
		String json;
		SdkURIBuilder uriBuilder = getURIBuilder();
		uriBuilder.setPathPoi(SdkURIBuilder.POI);
		
		try {
			uri = uriBuilder.build();
		} catch (URISyntaxException e) {
			throw new SdkDengueException("PoiProducer: An exception was thrown during"
					+ " url building", e);
		}

		
		GsonBuilder builder = new GsonBuilder();
		builder.registerTypeAdapter(Date.class, new DateJsonSerializer());
		builder.serializeSpecialFloatingPointValues();
		
		json = builder.create().toJson(poi);
		
		try {
			json = executePostRequest(uri, json);
			
		} catch (IOException e) {
			throw new SdkDengueException("\nPoiProducer: An exception was thrown during"
					+ " reques:\n" + json, e);
		}
		retPoi = new GsonDengueBuilder().build().fromJson(json, PointOfInterest.class);
		return retPoi;
		
	} 

}
