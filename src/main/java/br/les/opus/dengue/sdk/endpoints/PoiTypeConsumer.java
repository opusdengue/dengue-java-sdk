package br.les.opus.dengue.sdk.endpoints;

import java.io.IOException;
import java.lang.reflect.Type;
import java.net.URISyntaxException;
import java.util.List;

import br.les.opus.dengue.sdk.api.SdkDengueException;
import br.les.opus.dengue.sdk.domain.PoiType;
import br.les.opus.dengue.sdk.util.GsonDengueBuilder;
import br.les.opus.dengue.sdk.util.SdkURIBuilder;

import com.google.gson.reflect.TypeToken;

public class PoiTypeConsumer extends EndpointConsumer{
	private String language;
	
	public PoiTypeConsumer(String language){
		super();
		this.language = language;
	}
	
	public PoiTypeConsumer() {
		super();
		language = "pt";
	}
	
	/*
	 * Get all types of point of interests
	 */
	public List<PoiType> getAllPoiTypes(){
		String json;
		
		SdkURIBuilder uriBuilder = getURIBuilder();
		uriBuilder.setPathPoi(SdkURIBuilder.POITYPE); 
				
		try {
			json = executeGetRequest(uriBuilder.build(), language);
		} catch (IOException e) {
			throw new SdkDengueException("An exception was thrown during"
					+ " request", e);
		} catch (URISyntaxException e) {
			throw new SdkDengueException("An exception was thrown during"
					+ " uri creation", e);
		}
				
		Type type = new TypeToken<List<PoiType>>(){}.getType();
		
		return new GsonDengueBuilder().build().fromJson(json,type);
		
	}

}