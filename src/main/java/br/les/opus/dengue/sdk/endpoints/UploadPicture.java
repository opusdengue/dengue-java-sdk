package br.les.opus.dengue.sdk.endpoints;

import java.io.File;
import java.net.URL;
import java.util.List;

import br.les.opus.dengue.sdk.api.SdkDengueException;
import br.les.opus.dengue.sdk.domain.Picture;
import br.les.opus.dengue.sdk.util.GsonDengueBuilder;
import br.les.opus.dengue.sdk.util.MultipartUtility;
import br.les.opus.dengue.sdk.util.SdkURIBuilder;

public class UploadPicture extends EndpointConsumer{
	public UploadPicture() {
		super();
	}

	
	public Picture upload(File file){
		Picture picture = null;
		String charset = "UTF-8";
		SdkURIBuilder builder = getURIBuilder();
		builder.setPathPoi(SdkURIBuilder.PICTUREUPLOAD);
		URL url;
				
		try{
			url = builder.buildUrl();
		
			MultipartUtility mp = new MultipartUtility(url, charset);
			mp.addFilePart("file", file);
			
			List<String> response = mp.finish();
			
			picture = new GsonDengueBuilder().build().fromJson(response.get(0), Picture.class);
            
		}catch(Exception e){
			throw new SdkDengueException("Picture Upload: An exception was thrown during"
					+ " picture upload", e);
		}
		
		return picture;
			
	}
	

}
