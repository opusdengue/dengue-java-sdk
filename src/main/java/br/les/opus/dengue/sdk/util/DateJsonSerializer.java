package br.les.opus.dengue.sdk.util;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public class DateJsonSerializer implements JsonSerializer<Date>{

	public JsonElement serialize(Date src, Type typeOfSrc,
			JsonSerializationContext context) {
		
		SimpleDateFormat formatDate = new IsoDateFormat();		
		String sDate = formatDate.format(src);
		
		JsonPrimitive primitive = new JsonPrimitive(sDate);
				
		return primitive;
	}

}
