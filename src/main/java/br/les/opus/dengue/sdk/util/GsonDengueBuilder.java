package br.les.opus.dengue.sdk.util;


import java.util.Date;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class GsonDengueBuilder{

	public Gson build() {
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.registerTypeAdapter(Date.class, new IsoFormatDateDeserializer());
		
		return gsonBuilder.create();
	}
	
}
