package br.les.opus.dengue.sdk.util;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

public class IsoFormatDateDeserializer  implements JsonDeserializer<Date> {
	
	private SimpleDateFormat isoDateFormat;
	
	public IsoFormatDateDeserializer() {
		isoDateFormat = new IsoDateFormat();
	}
	
	public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		try {
			return isoDateFormat.parse(json.getAsString());
		} catch (ParseException e) {
			throw new JsonParseException(e.getMessage(), e);
		}
	}


}