package br.les.opus.dengue.sdk.util;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;


public class SdkURIBuilder {
	
	

	private StringBuffer uriBuilder;
	
	//private static String HOST = "localhost:1080";		//test
	private static String HOST = "vazadengue.inf.puc-rio.br/api";		//oficial
	//private static String HOST = "vazadengue.inf.puc-rio.br/desenv/api";		//desenv
	//private static String HOST = "10.123.74.235:8080/dengue-api";
	//private static String HOST = "192.168.0.12:8080/dengue-api";
	public static String POI = "/poi/";
	public static String ORDERBYDATA = "?sort=date&size=500";
	public static String POITYPE = "/poi-type";
	public static String PICTUREUPLOAD = "/picture/upload";
	public static String PICTUREDOWNLOAD = "/picture/{id}/thumb/{width}/{height}";
	public static final String TWEETS = "/tweet?sort=createdAt,desc&size=500";
	public static final String INSTAGRAM= "/instagram?size=500&sort=createdTime,desc&filter=location.latitude!=null";
	
	public SdkURIBuilder() {
		this.uriBuilder = new StringBuffer();
		this.uriBuilder.append("http://");
		this.uriBuilder.append(HOST);
	}
	
	public void setPathPoi(String path) {
		this.uriBuilder.append(path);
	}
	
	
	public URI build() throws URISyntaxException {
		return new URI(uriBuilder.toString());
	}
	
	public URL buildUrl() throws MalformedURLException {
		return new URL(uriBuilder.toString());
	}
	
	public String getUrlString(){
		return uriBuilder.toString();
	}

}
