package br.les.opus.dengue.sdk.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ PoiConsumerTest.class, PoiTypeConsumerTest.class, PoiProducerTest.class })
public class AllTests {

}
