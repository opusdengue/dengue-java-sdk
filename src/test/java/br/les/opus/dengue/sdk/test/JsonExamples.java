package br.les.opus.dengue.sdk.test;

public class JsonExamples {
	
	public static String getJsonAllPois(){
		return "{" +
				"\"links\": [" +
				"{" +
				"\"rel\": \"self\"," +
				"\"href\": \"http://dengue.les.inf.puc-rio.br/dengue-api-desenv/poi{?page,size,sort}\"" +
				"}" +
				"]," +
				"\"content\": [" +
				"{" +
				"\"id\": 82," +
				"\"location\": {" +
				"\"lat\": 45.3432501," +
				"\"lng\": 14.310129500000016" +
				"}," +
				"\"address\": \"Nova cesta, Opatija, Croatia\"," +
				"\"title\": \"titulo\"," +
				"\"description\": \"Teste\"," +
				"\"date\": \"2015-12-12T07:55:04.793Z\"," +
				"\"type\": {" +
				"\"id\": 2," +
				"\"name\": \"Pessoa com dengue\"" +
				"}," +
				"\"user\": {" +
				"\"id\": 27," +
				"\"name\": \"Diego\"," +
				"\"username\": \"diegocedrim@gmail.com\"," +
				"\"enabled\": true," +
				"\"locked\": false," +
				"\"roles\": null," +
				"\"resources\": null," +
				"\"shortName\": \"Diego\"," +
				"\"accountNonExpired\": true," +
				"\"accountNonLocked\": true," +
				"\"credentialsNonExpired\": true" +
				"}," +
				"\"pictures\": [" +
				"{" +
				"\"id\": 72," +
				"\"fileName\": \"ee34f229-2340-4463-a368-270bcbf45d2e-Dengue App Icon 1025.png\"," +
				"\"date\": \"2015-12-12T07:55:00.914Z\"," +
				"\"user\": {" +
				"\"id\": 27," +
				"\"name\": \"Diego\"," +
				"\"username\": \"diegocedrim@gmail.com\"," +
				"\"enabled\": true," +
				"\"locked\": false," +
				"\"roles\": null," +
				"\"resources\": null," +
				"\"shortName\": \"Diego\"," +
				"\"accountNonExpired\": true," +
				"\"accountNonLocked\": true," +
				"\"credentialsNonExpired\": true" +
				"}," +
				"\"mimeType\": \"image/png\"," +
				"\"width\": 1024," +
				"\"height\": 1024" +
				"}" +
				"]," +
				"\"upVoteCount\": 0," +
				"\"downVoteCount\": 0," +
				"\"userVote\": null," +
				"\"published\": true," +
				"\"links\": [" +
				"{" +
				"\"rel\": \"self\"," +
				"\"href\": \"http://dengue.les.inf.puc-rio.br/dengue-api-desenv/poi/82\"" +
				"}" +
				"]" +
				"}" +
				"]," +
				"\"page\": {" +
				"\"size\": 1," +
				"\"totalElements\": 1," +
				"\"totalPages\": 1," +
				"\"number\": 0" +
				"}" +
				"}";
	}
	
	public static String getJsonPoi(long id){
		return "{" + System.getProperty("line.separator") +
		"    \"id\": " + Long.toString(id) + "," + System.getProperty("line.separator") +
		"    \"location\": {" + System.getProperty("line.separator") +
		"        \"lat\": 37.422005," + System.getProperty("line.separator") +
		"        \"lng\": -122.084095" + System.getProperty("line.separator") +
		"    }," + System.getProperty("line.separator") +
		"    \"address\": \"Amphitheatre Pkwy, Mountain View, California, United States\"," + System.getProperty("line.separator") +
		"    \"title\": \"Emulador\"," + System.getProperty("line.separator") +
		"    \"description\": \"Teste do emulador - android jelly beans\"," + System.getProperty("line.separator") +
		"    \"date\": \"2015-12-11T23:44:10.057Z\"," + System.getProperty("line.separator") +
		"    \"type\": {" + System.getProperty("line.separator") +
		"        \"id\": 3," + System.getProperty("line.separator") +
		"        \"name\": \"Foco do mosquito\"" + System.getProperty("line.separator") +
		"    }," + System.getProperty("line.separator") +
		"    \"user\": null," + System.getProperty("line.separator") +
		"    \"pictures\": []," + System.getProperty("line.separator") +
		"    \"upVoteCount\": 0," + System.getProperty("line.separator") +
		"    \"downVoteCount\": 0," + System.getProperty("line.separator") +
		"    \"userVote\": null," + System.getProperty("line.separator") +
		"    \"published\": true," + System.getProperty("line.separator") +
		"    \"fieldValues\": [" + System.getProperty("line.separator") +
		"        {" + System.getProperty("line.separator") +
		"            \"id\": 414," + System.getProperty("line.separator") +
		"            \"value\": \"Yes\"," + System.getProperty("line.separator") +
		"            \"field\": {" + System.getProperty("line.separator") +
		"                \"id\": 21," + System.getProperty("line.separator") +
		"                \"name\": \"O foco se encontra em uma área pública?\"," + System.getProperty("line.separator") +
		"                \"helpText\": null," + System.getProperty("line.separator") +
		"                \"required\": true," + System.getProperty("line.separator") +
		"                \"type\": {" + System.getProperty("line.separator") +
		"                    \"id\": 1," + System.getProperty("line.separator") +
		"                    \"name\": \"Select\"" + System.getProperty("line.separator") +
		"                }," + System.getProperty("line.separator") +
		"                \"options\": [" + System.getProperty("line.separator") +
		"                    {" + System.getProperty("line.separator") +
		"                        \"id\": 171," + System.getProperty("line.separator") +
		"                        \"label\": \"Não\"," + System.getProperty("line.separator") +
		"                        \"value\": \"Não\"" + System.getProperty("line.separator") +
		"                    }," + System.getProperty("line.separator") +
		"                    {" + System.getProperty("line.separator") +
		"                        \"id\": 169," + System.getProperty("line.separator") +
		"                        \"label\": \"Não sei\"," + System.getProperty("line.separator") +
		"                        \"value\": \"Não informado\"" + System.getProperty("line.separator") +
		"                    }," + System.getProperty("line.separator") +
		"                    {" + System.getProperty("line.separator") +
		"                        \"id\": 170," + System.getProperty("line.separator") +
		"                        \"label\": \"Sim\"," + System.getProperty("line.separator") +
		"                        \"value\": \"Sim\"" + System.getProperty("line.separator") +
		"                    }" + System.getProperty("line.separator") +
		"                ]" + System.getProperty("line.separator") +
		"            }" + System.getProperty("line.separator") +
		"        }," + System.getProperty("line.separator") +
		"        {" + System.getProperty("line.separator") +
		"            \"id\": 415," + System.getProperty("line.separator") +
		"            \"value\": \"animal trough\"," + System.getProperty("line.separator") +
		"            \"field\": {" + System.getProperty("line.separator") +
		"                \"id\": 22," + System.getProperty("line.separator") +
		"                \"name\": \"Onde o foco foi encontrado?\"," + System.getProperty("line.separator") +
		"                \"helpText\": null," + System.getProperty("line.separator") +
		"                \"required\": true," + System.getProperty("line.separator") +
		"                \"type\": {" + System.getProperty("line.separator") +
		"                    \"id\": 1," + System.getProperty("line.separator") +
		"                    \"name\": \"Select\"" + System.getProperty("line.separator") +
		"                }," + System.getProperty("line.separator") +
		"                \"options\": [" + System.getProperty("line.separator") +
		"                    {" + System.getProperty("line.separator") +
		"                        \"id\": 180," + System.getProperty("line.separator") +
		"                        \"label\": \"aquário\"," + System.getProperty("line.separator") +
		"                        \"value\": \"aquário\"" + System.getProperty("line.separator") +
		"                    }," + System.getProperty("line.separator") +
		"                    {" + System.getProperty("line.separator") +
		"                        \"id\": 181," + System.getProperty("line.separator") +
		"                        \"label\": \"bebedouro de animal\"," + System.getProperty("line.separator") +
		"                        \"value\": \"bebedouro de animal\"" + System.getProperty("line.separator") +
		"                    }," + System.getProperty("line.separator") +
		"                    {" + System.getProperty("line.separator") +
		"                        \"id\": 176," + System.getProperty("line.separator") +
		"                        \"label\": \"calha\"," + System.getProperty("line.separator") +
		"                        \"value\": \"calha\"" + System.getProperty("line.separator") +
		"                    }," + System.getProperty("line.separator") +
		"                    {" + System.getProperty("line.separator") +
		"                        \"id\": 178," + System.getProperty("line.separator") +
		"                        \"label\": \"córrego ou riacho\"," + System.getProperty("line.separator") +
		"                        \"value\": \"córrego ou riacho\"" + System.getProperty("line.separator") +
		"                    }," + System.getProperty("line.separator") +
		"                    {" + System.getProperty("line.separator") +
		"                        \"id\": 177," + System.getProperty("line.separator") +
		"                        \"label\": \"embalagens vazias (garrafas, latas...)\"," + System.getProperty("line.separator") +
		"                        \"value\": \"embalagens vazias (garrafas, latas...)\"" + System.getProperty("line.separator") +
		"                    }," + System.getProperty("line.separator") +
		"                    {" + System.getProperty("line.separator") +
		"                        \"id\": 182," + System.getProperty("line.separator") +
		"                        \"label\": \"outro\"," + System.getProperty("line.separator") +
		"                        \"value\": \"outro\"" + System.getProperty("line.separator") +
		"                    }," + System.getProperty("line.separator") +
		"                    {" + System.getProperty("line.separator") +
		"                        \"id\": 172," + System.getProperty("line.separator") +
		"                        \"label\": \"piscina abandonada\"," + System.getProperty("line.separator") +
		"                        \"value\": \"piscina abandonada\"" + System.getProperty("line.separator") +
		"                    }," + System.getProperty("line.separator") +
		"                    {" + System.getProperty("line.separator") +
		"                        \"id\": 174," + System.getProperty("line.separator") +
		"                        \"label\": \"pneu abandonado\"," + System.getProperty("line.separator") +
		"                        \"value\": \"pneu abandonado\"" + System.getProperty("line.separator") +
		"                    }," + System.getProperty("line.separator") +
		"                    {" + System.getProperty("line.separator") +
		"                        \"id\": 179," + System.getProperty("line.separator") +
		"                        \"label\": \"ralo\"," + System.getProperty("line.separator") +
		"                        \"value\": \"ralo\"" + System.getProperty("line.separator") +
		"                    }," + System.getProperty("line.separator") +
		"                    {" + System.getProperty("line.separator") +
		"                        \"id\": 173," + System.getProperty("line.separator") +
		"                        \"label\": \"terreno baldio\"," + System.getProperty("line.separator") +
		"                        \"value\": \"terreno baldio\"" + System.getProperty("line.separator") +
		"                    }," + System.getProperty("line.separator") +
		"                    {" + System.getProperty("line.separator") +
		"                        \"id\": 175," + System.getProperty("line.separator") +
		"                        \"label\": \"vaso de planta\"," + System.getProperty("line.separator") +
		"                        \"value\": \"vaso de planta\"" + System.getProperty("line.separator") +
		"                    }" + System.getProperty("line.separator") +
		"                ]" + System.getProperty("line.separator") +
		"            }" + System.getProperty("line.separator") +
		"        }" + System.getProperty("line.separator") +
		"    ]" + System.getProperty("line.separator") +
		"}";
	}
	
	public static String getJsonPoi(){
		return "{" + System.getProperty("line.separator") +
		"    \"id\": 1," + System.getProperty("line.separator") +
		"    \"location\": {" + System.getProperty("line.separator") +
		"        \"lat\": 37.422005," + System.getProperty("line.separator") +
		"        \"lng\": -122.084095" + System.getProperty("line.separator") +
		"    }," + System.getProperty("line.separator") +
		"    \"address\": \"Amphitheatre Pkwy, Mountain View, California, United States\"," + System.getProperty("line.separator") +
		"    \"title\": \"Emulador\"," + System.getProperty("line.separator") +
		"    \"description\": \"Teste do emulador - android jelly beans\"," + System.getProperty("line.separator") +
		"    \"date\": \"2015-12-11T23:44:10.057Z\"," + System.getProperty("line.separator") +
		"    \"type\": {" + System.getProperty("line.separator") +
		"        \"id\": 3," + System.getProperty("line.separator") +
		"        \"name\": \"Foco do mosquito\"" + System.getProperty("line.separator") +
		"    }," + System.getProperty("line.separator") +
		"    \"user\": null," + System.getProperty("line.separator") +
		"    \"pictures\": []," + System.getProperty("line.separator") +
		"    \"upVoteCount\": 0," + System.getProperty("line.separator") +
		"    \"downVoteCount\": 0," + System.getProperty("line.separator") +
		"    \"userVote\": null," + System.getProperty("line.separator") +
		"    \"published\": true," + System.getProperty("line.separator") +
		"    \"fieldValues\": [" + System.getProperty("line.separator") +
		"        {" + System.getProperty("line.separator") +
		"            \"id\": 414," + System.getProperty("line.separator") +
		"            \"value\": \"Yes\"," + System.getProperty("line.separator") +
		"            \"field\": {" + System.getProperty("line.separator") +
		"                \"id\": 21," + System.getProperty("line.separator") +
		"                \"name\": \"O foco se encontra em uma área pública?\"," + System.getProperty("line.separator") +
		"                \"helpText\": null," + System.getProperty("line.separator") +
		"                \"required\": true," + System.getProperty("line.separator") +
		"                \"type\": {" + System.getProperty("line.separator") +
		"                    \"id\": 1," + System.getProperty("line.separator") +
		"                    \"name\": \"Select\"" + System.getProperty("line.separator") +
		"                }," + System.getProperty("line.separator") +
		"                \"options\": [" + System.getProperty("line.separator") +
		"                    {" + System.getProperty("line.separator") +
		"                        \"id\": 171," + System.getProperty("line.separator") +
		"                        \"label\": \"Não\"," + System.getProperty("line.separator") +
		"                        \"value\": \"Não\"" + System.getProperty("line.separator") +
		"                    }," + System.getProperty("line.separator") +
		"                    {" + System.getProperty("line.separator") +
		"                        \"id\": 169," + System.getProperty("line.separator") +
		"                        \"label\": \"Não sei\"," + System.getProperty("line.separator") +
		"                        \"value\": \"Não informado\"" + System.getProperty("line.separator") +
		"                    }," + System.getProperty("line.separator") +
		"                    {" + System.getProperty("line.separator") +
		"                        \"id\": 170," + System.getProperty("line.separator") +
		"                        \"label\": \"Sim\"," + System.getProperty("line.separator") +
		"                        \"value\": \"Sim\"" + System.getProperty("line.separator") +
		"                    }" + System.getProperty("line.separator") +
		"                ]" + System.getProperty("line.separator") +
		"            }" + System.getProperty("line.separator") +
		"        }," + System.getProperty("line.separator") +
		"        {" + System.getProperty("line.separator") +
		"            \"id\": 415," + System.getProperty("line.separator") +
		"            \"value\": \"animal trough\"," + System.getProperty("line.separator") +
		"            \"field\": {" + System.getProperty("line.separator") +
		"                \"id\": 22," + System.getProperty("line.separator") +
		"                \"name\": \"Onde o foco foi encontrado?\"," + System.getProperty("line.separator") +
		"                \"helpText\": null," + System.getProperty("line.separator") +
		"                \"required\": true," + System.getProperty("line.separator") +
		"                \"type\": {" + System.getProperty("line.separator") +
		"                    \"id\": 1," + System.getProperty("line.separator") +
		"                    \"name\": \"Select\"" + System.getProperty("line.separator") +
		"                }," + System.getProperty("line.separator") +
		"                \"options\": [" + System.getProperty("line.separator") +
		"                    {" + System.getProperty("line.separator") +
		"                        \"id\": 180," + System.getProperty("line.separator") +
		"                        \"label\": \"aquário\"," + System.getProperty("line.separator") +
		"                        \"value\": \"aquário\"" + System.getProperty("line.separator") +
		"                    }," + System.getProperty("line.separator") +
		"                    {" + System.getProperty("line.separator") +
		"                        \"id\": 181," + System.getProperty("line.separator") +
		"                        \"label\": \"bebedouro de animal\"," + System.getProperty("line.separator") +
		"                        \"value\": \"bebedouro de animal\"" + System.getProperty("line.separator") +
		"                    }," + System.getProperty("line.separator") +
		"                    {" + System.getProperty("line.separator") +
		"                        \"id\": 176," + System.getProperty("line.separator") +
		"                        \"label\": \"calha\"," + System.getProperty("line.separator") +
		"                        \"value\": \"calha\"" + System.getProperty("line.separator") +
		"                    }," + System.getProperty("line.separator") +
		"                    {" + System.getProperty("line.separator") +
		"                        \"id\": 178," + System.getProperty("line.separator") +
		"                        \"label\": \"córrego ou riacho\"," + System.getProperty("line.separator") +
		"                        \"value\": \"córrego ou riacho\"" + System.getProperty("line.separator") +
		"                    }," + System.getProperty("line.separator") +
		"                    {" + System.getProperty("line.separator") +
		"                        \"id\": 177," + System.getProperty("line.separator") +
		"                        \"label\": \"embalagens vazias (garrafas, latas...)\"," + System.getProperty("line.separator") +
		"                        \"value\": \"embalagens vazias (garrafas, latas...)\"" + System.getProperty("line.separator") +
		"                    }," + System.getProperty("line.separator") +
		"                    {" + System.getProperty("line.separator") +
		"                        \"id\": 182," + System.getProperty("line.separator") +
		"                        \"label\": \"outro\"," + System.getProperty("line.separator") +
		"                        \"value\": \"outro\"" + System.getProperty("line.separator") +
		"                    }," + System.getProperty("line.separator") +
		"                    {" + System.getProperty("line.separator") +
		"                        \"id\": 172," + System.getProperty("line.separator") +
		"                        \"label\": \"piscina abandonada\"," + System.getProperty("line.separator") +
		"                        \"value\": \"piscina abandonada\"" + System.getProperty("line.separator") +
		"                    }," + System.getProperty("line.separator") +
		"                    {" + System.getProperty("line.separator") +
		"                        \"id\": 174," + System.getProperty("line.separator") +
		"                        \"label\": \"pneu abandonado\"," + System.getProperty("line.separator") +
		"                        \"value\": \"pneu abandonado\"" + System.getProperty("line.separator") +
		"                    }," + System.getProperty("line.separator") +
		"                    {" + System.getProperty("line.separator") +
		"                        \"id\": 179," + System.getProperty("line.separator") +
		"                        \"label\": \"ralo\"," + System.getProperty("line.separator") +
		"                        \"value\": \"ralo\"" + System.getProperty("line.separator") +
		"                    }," + System.getProperty("line.separator") +
		"                    {" + System.getProperty("line.separator") +
		"                        \"id\": 173," + System.getProperty("line.separator") +
		"                        \"label\": \"terreno baldio\"," + System.getProperty("line.separator") +
		"                        \"value\": \"terreno baldio\"" + System.getProperty("line.separator") +
		"                    }," + System.getProperty("line.separator") +
		"                    {" + System.getProperty("line.separator") +
		"                        \"id\": 175," + System.getProperty("line.separator") +
		"                        \"label\": \"vaso de planta\"," + System.getProperty("line.separator") +
		"                        \"value\": \"vaso de planta\"" + System.getProperty("line.separator") +
		"                    }" + System.getProperty("line.separator") +
		"                ]" + System.getProperty("line.separator") +
		"            }" + System.getProperty("line.separator") +
		"        }" + System.getProperty("line.separator") +
		"    ]" + System.getProperty("line.separator") +
		"}";
	}
	
	public static String getAllTweets(){
		return "{" + System.getProperty("line.separator") + 
				"    \"links\": [" + System.getProperty("line.separator") + 
				"        {" + System.getProperty("line.separator") + 
				"            \"rel\": \"next\"," + System.getProperty("line.separator") + 
				"            \"href\": \"http://dengue.les.inf.puc-rio.br/dengue-api-desenv/tweet?page=1&size=1&sort=createdAt,desc\"" + System.getProperty("line.separator") + 
				"        }," + System.getProperty("line.separator") + 
				"        {" + System.getProperty("line.separator") + 
				"            \"rel\": \"self\"," + System.getProperty("line.separator") + 
				"            \"href\": \"http://dengue.les.inf.puc-rio.br/dengue-api-desenv/tweet{?page,size,sort}\"" + System.getProperty("line.separator") + 
				"        }" + System.getProperty("line.separator") + 
				"    ]," + System.getProperty("line.separator") + 
				"    \"content\": [" + System.getProperty("line.separator") + 
				"        {" + System.getProperty("line.separator") + 
				"            \"id\": 674727878173569000," + System.getProperty("line.separator") + 
				"            \"createdAt\": \"2015-12-09T18:10:36.000Z\"," + System.getProperty("line.separator") + 
				"            \"geolocation\": {" + System.getProperty("line.separator") + 
				"                \"lat\": -8.05," + System.getProperty("line.separator") + 
				"                \"lng\": -34.9" + System.getProperty("line.separator") + 
				"            }," + System.getProperty("line.separator") + 
				"            \"text\": \"Cuidado com a dengue. @ Recife, Brazil https://t.co/5fJREIePSC\"," + System.getProperty("line.separator") + 
				"            \"user\": {" + System.getProperty("line.separator") + 
				"                \"id\": 1161463712," + System.getProperty("line.separator") + 
				"                \"name\": \"Ritchelly Pinto\"," + System.getProperty("line.separator") + 
				"                \"screenName\": \"RitchellyPinto\"," + System.getProperty("line.separator") + 
				"                \"location\": null," + System.getProperty("line.separator") + 
				"                \"profileImageUrl\": \"http://pbs.twimg.com/profile_images/3765555885/9b1031d196269d66f21aaa78709ec6b1_normal.jpeg\"," + System.getProperty("line.separator") + 
				"                \"profileImageUrlHttps\": \"https://pbs.twimg.com/profile_images/3765555885/9b1031d196269d66f21aaa78709ec6b1_normal.jpeg\"," + System.getProperty("line.separator") + 
				"                \"url\": \"http://ritchelly.jusbrasil.com.br/\"" + System.getProperty("line.separator") + 
				"            }," + System.getProperty("line.separator") + 
				"            \"classification\": {" + System.getProperty("line.separator") + 
				"                \"id\": 1," + System.getProperty("line.separator") + 
				"                \"key\": \"INFORMATIVE\"," + System.getProperty("line.separator") + 
				"                \"label\": \"Informativo\"," + System.getProperty("line.separator") + 
				"                \"description\": null" + System.getProperty("line.separator") + 
				"            }," + System.getProperty("line.separator") + 
				"            \"links\": [" + System.getProperty("line.separator") + 
				"                {" + System.getProperty("line.separator") + 
				"                    \"rel\": \"self\"," + System.getProperty("line.separator") + 
				"                    \"href\": \"http://dengue.les.inf.puc-rio.br/dengue-api-desenv/tweet/674727878173569024\"" + System.getProperty("line.separator") + 
				"                }" + System.getProperty("line.separator") + 
				"            ]" + System.getProperty("line.separator") + 
				"        }" + System.getProperty("line.separator") + 
				"    ]," + System.getProperty("line.separator") + 
				"    \"page\": {" + System.getProperty("line.separator") + 
				"        \"size\": 1," + System.getProperty("line.separator") + 
				"        \"totalElements\": 463," + System.getProperty("line.separator") + 
				"        \"totalPages\": 463," + System.getProperty("line.separator") + 
				"        \"number\": 0" + System.getProperty("line.separator") + 
				"    }" + System.getProperty("line.separator") + 
				"}";
	}
	
	public static String getAllInstagramPosts(){
		return "{" + System.getProperty("line.separator") + 
				"    \"links\": [" + System.getProperty("line.separator") + 
				"        {" + System.getProperty("line.separator") + 
				"            \"rel\": \"next\"," + System.getProperty("line.separator") + 
				"            \"href\": \"http://dengue.les.inf.puc-rio.br/dengue-api-desenv/instagram?page=1&size=1&sort=createdTime,desc\"" + System.getProperty("line.separator") + 
				"        }," + System.getProperty("line.separator") + 
				"        {" + System.getProperty("line.separator") + 
				"            \"rel\": \"self\"," + System.getProperty("line.separator") + 
				"            \"href\": \"http://dengue.les.inf.puc-rio.br/dengue-api-desenv/instagram{?page,size,sort}\"" + System.getProperty("line.separator") + 
				"        }" + System.getProperty("line.separator") + 
				"    ]," + System.getProperty("line.separator") + 
				"    \"content\": [" + System.getProperty("line.separator") + 
				"        {" + System.getProperty("line.separator") + 
				"            \"id\": \"1136768595746684917_1989749926\"," + System.getProperty("line.separator") + 
				"            \"type\": \"image\"," + System.getProperty("line.separator") + 
				"            \"filter\": \"Juno\"," + System.getProperty("line.separator") + 
				"            \"caption\": {" + System.getProperty("line.separator") + 
				"                \"id\": 1136768624075014100," + System.getProperty("line.separator") + 
				"                \"createdTime\": \"2015-12-10T02:43:18.000Z\"," + System.getProperty("line.separator") + 
				"                \"text\": \"Semana de #chikungunya v. 2.0 #laterceraeslavencida ☠⚰🤒🔚\"" + System.getProperty("line.separator") + 
				"            }," + System.getProperty("line.separator") + 
				"            \"link\": \"https://www.instagram.com/p/_GnQPVTP_1/\"," + System.getProperty("line.separator") + 
				"            \"user\": {" + System.getProperty("line.separator") + 
				"                \"id\": 1989749926," + System.getProperty("line.separator") + 
				"                \"username\": \"erchhi\"," + System.getProperty("line.separator") + 
				"                \"fullName\": \"ErikaA Pudwill Solis Castañeda\"," + System.getProperty("line.separator") + 
				"                \"profilePictureUrl\": \"https://igcdn-photos-a-a.akamaihd.net/hphotos-ak-xap1/t51.2885-19/11427358_1567614336837096_1401661484_a.jpg\"" + System.getProperty("line.separator") + 
				"            }," + System.getProperty("line.separator") + 
				"            \"createdTime\": \"2015-12-10T02:43:18.000Z\"," + System.getProperty("line.separator") + 
				"            \"images\": {" + System.getProperty("line.separator") + 
				"                \"id\": 102555," + System.getProperty("line.separator") + 
				"                \"lowResolution\": {" + System.getProperty("line.separator") + 
				"                    \"id\": 307597," + System.getProperty("line.separator") + 
				"                    \"url\": \"https://scontent.cdninstagram.com/hphotos-xaf1/t51.2885-15/s320x320/e35/12357801_178582469156343_1374966657_n.jpg\"," + System.getProperty("line.separator") + 
				"                    \"width\": 320," + System.getProperty("line.separator") + 
				"                    \"height\": 320," + System.getProperty("line.separator") + 
				"                    \"valid\": true" + System.getProperty("line.separator") + 
				"                }," + System.getProperty("line.separator") + 
				"                \"thumbnail\": {" + System.getProperty("line.separator") + 
				"                    \"id\": 307599," + System.getProperty("line.separator") + 
				"                    \"url\": \"https://scontent.cdninstagram.com/hphotos-xpa1/t51.2885-15/s150x150/e35/c0.103.1080.1080/12346069_995187750538714_1548099227_n.jpg\"," + System.getProperty("line.separator") + 
				"                    \"width\": 150," + System.getProperty("line.separator") + 
				"                    \"height\": 150," + System.getProperty("line.separator") + 
				"                    \"valid\": true" + System.getProperty("line.separator") + 
				"                }," + System.getProperty("line.separator") + 
				"                \"standardResolution\": {" + System.getProperty("line.separator") + 
				"                    \"id\": 307598," + System.getProperty("line.separator") + 
				"                    \"url\": \"https://scontent.cdninstagram.com/hphotos-xaf1/t51.2885-15/s640x640/sh0.08/e35/12357801_178582469156343_1374966657_n.jpg\"," + System.getProperty("line.separator") + 
				"                    \"width\": 640," + System.getProperty("line.separator") + 
				"                    \"height\": 640," + System.getProperty("line.separator") + 
				"                    \"valid\": true" + System.getProperty("line.separator") + 
				"                }," + System.getProperty("line.separator") + 
				"                \"valid\": true" + System.getProperty("line.separator") + 
				"            }," + System.getProperty("line.separator") + 
				"            \"videos\": null," + System.getProperty("line.separator") + 
				"            \"location\": {" + System.getProperty("line.separator") + 
				"                \"id\": 17172," + System.getProperty("line.separator") + 
				"                \"name\": \"San Román\"," + System.getProperty("line.separator") + 
				"                \"latitude\": 18.880144309," + System.getProperty("line.separator") + 
				"                \"longitude\": -96.966627118" + System.getProperty("line.separator") + 
				"            }," + System.getProperty("line.separator") + 
				"            \"valid\": true," + System.getProperty("line.separator") + 
				"            \"links\": [" + System.getProperty("line.separator") + 
				"                {" + System.getProperty("line.separator") + 
				"                    \"rel\": \"self\"," + System.getProperty("line.separator") + 
				"                    \"href\": \"http://dengue.les.inf.puc-rio.br/dengue-api-desenv/instagram/1136768595746684917_1989749926\"" + System.getProperty("line.separator") + 
				"                }" + System.getProperty("line.separator") + 
				"            ]" + System.getProperty("line.separator") + 
				"        }" + System.getProperty("line.separator") + 
				"    ]," + System.getProperty("line.separator") + 
				"    \"page\": {" + System.getProperty("line.separator") + 
				"        \"size\": 1," + System.getProperty("line.separator") + 
				"        \"totalElements\": 16586," + System.getProperty("line.separator") + 
				"        \"totalPages\": 16586," + System.getProperty("line.separator") + 
				"        \"number\": 0" + System.getProperty("line.separator") + 
				"    }" + System.getProperty("line.separator") + 
				"}";
	}
	
	public static String getJsonPoiType(){
		return "[" +
				"{" + 
				"\"id\": \"2\"," + 
				"\"name\": \"Pessoa com dengue\"," + 
				"\"fields\": [" + 
				"{" + 
				"\"id\": 1," + 
				"\"name\": \"Idade\"," + 
				"\"helpText\": \"Informe a idade (mesmo que seja apenas aproximada) do paciente\"," + 
				"\"required\": true," + 
				"\"type\": {" + 
				"\"id\": 2," + 
				"\"name\": \"Number\"" + 
				"}" + 
				"}]" + 
				"}" + 
				"]";
		
	}

}
