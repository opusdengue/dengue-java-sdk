package br.les.opus.dengue.sdk.test;

import static org.junit.Assert.assertEquals;
import static org.mockserver.integration.ClientAndServer.startClientAndServer;
import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;

import java.util.ArrayList;
import java.util.List;

import io.netty.handler.codec.http.HttpHeaders;

import org.junit.After;
import org.junit.Before;
import org.mockserver.integration.ClientAndServer;
import org.mockserver.model.Header;
import org.mockserver.model.Parameter;

import br.les.opus.dengue.sdk.domain.Envelope;
import br.les.opus.dengue.sdk.domain.InstagramEnvelope;
import br.les.opus.dengue.sdk.domain.PointOfInterest;
import br.les.opus.dengue.sdk.domain.TweetEnvelope;
import br.les.opus.dengue.sdk.endpoints.PoiConsumer;

public class PoiConsumerTest {
	private ClientAndServer mockServer;

		
	@Before
	public void  startProxy() {
	    mockServer = startClientAndServer(1080);
	}
	
	@After
	public void stopProxy() {
	    mockServer.stop();
	}
	
	@org.junit.Test
	public void testGetAllPointsOfInterest() {
		List<Parameter> parametros = new ArrayList<Parameter>();
		parametros.add(new Parameter("sort", "date"));
		parametros.add(new Parameter("size", "500"));
		
		mockServer
		.when(request().withPath("/poi/")
				.withQueryStringParameters(
                        parametros
                ))
		.respond(response()
				.withHeader(
						new Header(HttpHeaders.Names.CONTENT_TYPE, "application/json")
						)
				.withBody(JsonExamples.getJsonAllPois()));
		
		
		PoiConsumer consumer = new PoiConsumer();
		
		Envelope<PointOfInterest> pois = consumer.getAllPointsOfInterest();
		
		assertEquals("titulo", pois.getContent().get(0).getTitle());
		
		
	}
	
	@org.junit.Test
	public void testGetPointOfInterest() {
		long id = 80L;
		
		mockServer
		.when(request().withPath("/poi/" + Long.toString(id)))
		.respond(response()
				.withHeader(
						new Header(HttpHeaders.Names.CONTENT_TYPE, "application/json")
						)
				.withBody(JsonExamples.getJsonPoi(id)));
		
		
		PoiConsumer consumer = new PoiConsumer();
		
		PointOfInterest poi = consumer.getPointofInterest(id);
		
		assertEquals(80L, (long)poi.getId());
	}
	
	@org.junit.Test
	public void testGetTweets() {
		List<Parameter> parametros = new ArrayList<Parameter>();
		parametros.add(new Parameter("sort", "createdAt,desc"));
		parametros.add(new Parameter("size", "500"));
		
		long id = 674727878173569000L;
		
		mockServer
		.when(request().withPath("/tweet")
				.withQueryStringParameters(parametros))
		.respond(response()
				.withHeader(
						new Header(HttpHeaders.Names.CONTENT_TYPE, "application/json")
						)
				.withBody(JsonExamples.getAllTweets()));
		
		
		PoiConsumer consumer = new PoiConsumer();
		
		TweetEnvelope tweets = consumer.getAllTweets();
		
		assertEquals(id, (long)tweets.getContent().get(0).getId());
	}
	
	@org.junit.Test
	public void testGetInstagramPosts() {
		List<Parameter> parametros = new ArrayList<Parameter>();
		parametros.add(new Parameter("sort", "createdTime,desc"));
		parametros.add(new Parameter("size", "500"));
		parametros.add(new Parameter("filter", "location.latitude!=null"));
		
		String id = "1136768595746684917_1989749926";
		
		mockServer
		.when(request().withPath("/instagram")
				.withQueryStringParameters(parametros))
		.respond(response()
				.withHeader(
						new Header(HttpHeaders.Names.CONTENT_TYPE, "application/json")
						)
				.withBody(JsonExamples.getAllInstagramPosts()));
		
		
		PoiConsumer consumer = new PoiConsumer();
		
		InstagramEnvelope envelope = consumer.getAllInstagramPosts();
		
		assertEquals(id, envelope.getContent().get(0).getId());
	}

}
