package br.les.opus.dengue.sdk.test;

import static org.junit.Assert.assertEquals;
import static org.mockserver.integration.ClientAndServer.startClientAndServer;
import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockserver.integration.ClientAndServer;
import org.mockserver.model.Header;

import br.les.opus.dengue.sdk.domain.Denunciation;
import br.les.opus.dengue.sdk.domain.LatLng;
import br.les.opus.dengue.sdk.domain.Picture;
import br.les.opus.dengue.sdk.domain.PoiType;
import br.les.opus.dengue.sdk.domain.PointOfInterest;
import br.les.opus.dengue.sdk.domain.fields.Field;
import br.les.opus.dengue.sdk.domain.fields.FieldType;
import br.les.opus.dengue.sdk.endpoints.PoiProducer;
import io.netty.handler.codec.http.HttpHeaders;

public class PoiProducerTest {
	private ClientAndServer mockServer;

	
	@Before
	public void  startProxy() {
	    mockServer = startClientAndServer(1080);
	}
	
	@After
	public void stopProxy() {
	    mockServer.stop();
	}
	

	@Test
	public void testProducer() {
		PointOfInterest poi = new PointOfInterest();
		poi.setAddress("los angeles");
		poi.setDate(new GregorianCalendar().getTime());
		poi.setTitle("titulo");
		poi.setPublished(true);
		poi.setDescription("descricao");
		
		PoiType type = new PoiType();
		type.setId(1L);
		type.setName("type");
		
		List<Field> fields = new ArrayList<Field>();
		Field f = new Field();
		f.setId(1L);
		f.setName("field");
		f.setHelpText("help");
		f.setRequired(true);
		FieldType fType = new FieldType();
		fType.setId(1L);
		fType.setName("fieldtype");
		f.setType(fType);
		fields.add(f);
		type.setFields(fields);
		
		poi.setLocation(new LatLng(4.5,  4.54));
		
		List<Picture> pictures = new ArrayList<Picture>();
		poi.setPictures(pictures);
		
		poi.setDenunciations((List<Denunciation>)new ArrayList<Denunciation>());
		poi.setType(type);
		
		
		mockServer
		.when(request().withMethod("POST")
				.withPath("/poi/"))
		.respond(response()
				.withHeader(
						new Header(HttpHeaders.Names.CONTENT_TYPE, "application/json")
						)
				.withBody(JsonExamples.getJsonPoi()));
		
		PoiProducer producer = new PoiProducer();
		
		PointOfInterest poiRes = producer.sendPoiAnonymous(poi);
		
		assertEquals(1L, (long)poiRes.getId());
	}

}
