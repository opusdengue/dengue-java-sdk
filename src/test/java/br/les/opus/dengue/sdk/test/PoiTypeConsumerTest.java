package br.les.opus.dengue.sdk.test;

import static org.junit.Assert.assertEquals;
import static org.mockserver.integration.ClientAndServer.startClientAndServer;
import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;
import io.netty.handler.codec.http.HttpHeaders;

import org.junit.After;
import org.junit.Before;
import org.mockserver.integration.ClientAndServer;
import org.mockserver.model.Header;

import br.les.opus.dengue.sdk.endpoints.PoiTypeConsumer;

public class PoiTypeConsumerTest {
	private ClientAndServer mockServer;

		
	@Before
	public void  startProxy() {
	    mockServer = startClientAndServer(1080);
	}
	
	@After
	public void stopProxy() {
	    mockServer.stop();
	}
	
	@org.junit.Test
	public void testConsumer() {
		mockServer
		.when(request().withPath("/poi-type"))
		.respond(response()
				.withHeader(
						new Header(HttpHeaders.Names.CONTENT_TYPE, "application/json")
						)
				.withBody(JsonExamples.getJsonPoiType()));
		
		
		PoiTypeConsumer consumer = new PoiTypeConsumer();
		
		consumer.getAllPoiTypes();
		
		assertEquals("Pessoa com dengue", consumer.getAllPoiTypes().get(0).getName());
		
	}
	

}
